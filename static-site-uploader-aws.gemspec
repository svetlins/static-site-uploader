Gem::Specification.new do |s|
  s.name        = 'static-site-uploader-aws'
  s.version     = '0.0.1'
  s.licenses    = ['MIT']
  s.summary     = ""
  s.description = ""
  s.authors     = ["Svetlin Simonyan"]
  s.email       = 'svetlins@gmail.com'
  s.files       = ["lib/**/*.rb"]
  s.executables = ["site-uploader"]
  s.homepage    = ''
  s.metadata    = {}
  s.add_runtime_dependency 'pry'
  s.add_runtime_dependency 'aws-sdk', '~> 3.0'
  s.add_runtime_dependency 'inifile', '~> 3.0'
  s.add_runtime_dependency 'mimemagic', '~> 0.3'
  s.add_runtime_dependency 'terminal-table', '~> 1.8.0'
end
