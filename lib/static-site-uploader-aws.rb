require 'inifile'
require 'static-site-uploader-aws/config'
require 'static-site-uploader-aws/error'
require 'static-site-uploader-aws/commands'
require 'aws-sdk'
require 'pry'
require 'mimemagic'
require 'terminal-table'

class StaticSiteUploaderAws
  attr_reader :config

  def initialize(config)
    @config = config
  end

  def create_bucket
    s3 = Aws::S3::Resource.new(
      access_key_id: config.access_key_id,
      secret_access_key: config.secret_access_key,
      region: config.region
    )

    bucket = s3.bucket(config.bucket_name)

    begin
      bucket.delete! if bucket.exists?
    rescue Aws::S3::Errors::PermanentRedirect
      raise Error, "Bucket #{bucket.name} is inaccessible. Possible reasons: the name is owned by another AWS account or is in another region"
    end

    bucket.create

    bucket.website.put website_configuration: {
                         index_document: {
                           suffix: "index.html",
                         },
                       }
  end

  def upload_site
    s3 = Aws::S3::Resource.new(
      access_key_id: config.access_key_id,
      secret_access_key: config.secret_access_key,
      region: config.region
    )

    bucket = s3.bucket(config.bucket_name)

    Dir["#{config.site_directory}/**/*"].each do |file_name|
      next if File.directory?(file_name)

      key = file_name.
              delete_prefix(config.site_directory).
              delete_prefix('/')

      mime_type = if key.split('.').size > 1
                    MimeMagic.by_extension(key.split('.').last)&.type
                  end

      bucket.put_object key: key,
                        acl: 'public-read',
                        body: File.read(file_name),
                        content_type: mime_type

      print '.'
    end

    puts
  end

  def invalidate_distribution
    cloud_front.create_invalidation(
      {
        distribution_id: config.distribution_id,
        invalidation_batch: {
          paths: {
            quantity: 1,
            items: ["/*"],
          },
          caller_reference: SecureRandom.hex(10)
        }
      }
    )
  end

  def create_distribution
    origin_id = SecureRandom.hex(10)

    create_distribution_result = cloud_front.create_distribution(
      distribution_config: {
        viewer_certificate: {
          cloud_front_default_certificate: false,
          iam_certificate_id: nil,
          ssl_support_method: "sni-only",
          minimum_protocol_version: "TLSv1.1_2016",
          acm_certificate_arn: config.certificate_arn,
          certificate: config.certificate_arn,
          certificate_source: "acm"
        },
        caller_reference: SecureRandom.hex(10),
        default_cache_behavior: {
          :target_origin_id=>origin_id,
          :forwarded_values=>{
            :query_string=>false,
            :cookies=>{:forward=>"none"},
            :headers=>{:quantity=>0},
            :query_string_cache_keys=>{:quantity=>0}
          },
          :trusted_signers=>{:enabled=>false, :quantity=>0},
          :viewer_protocol_policy=>"redirect-to-https",
          :min_ttl=>0,
          :allowed_methods=>{
            :quantity=>2,
            :items=>["HEAD", "GET"],
            :cached_methods=>{:quantity=>2, :items=>["HEAD", "GET"]}
          },
          :smooth_streaming=>false,
          :default_ttl=>86400,
          :max_ttl=>31536000,
          :compress=>false,
          :lambda_function_associations=>{:quantity=>0},
          :field_level_encryption_id=>""
        },
        aliases: {
          quantity: 1,
          items: [config.target_domain]
        },
        origins: {
          :quantity=>1,
          :items=>
          [{:id=>origin_id,
            :domain_name=>URI.parse(s3_url).hostname,
            :origin_path=>"",
            :custom_headers=>{:quantity=>0},
            :custom_origin_config=>
            {:http_port=>80,
             :https_port=>443,
             :origin_protocol_policy=>"http-only",
             :origin_ssl_protocols=>
             {:quantity=>3, :items=>["TLSv1", "TLSv1.1", "TLSv1.2"]},
             :origin_read_timeout=>30,
             :origin_keepalive_timeout=>5}}]
        },
        comment: 'Programatically created by upload.rb',
        enabled: true,
      }
    )

    distribution_id = create_distribution_result.distribution.id
    cdn_domain = cloud_front.get_distribution(id: distribution_id).distribution.domain_name

    {
      distribution_id: distribution_id,
      cdn_domain: cdn_domain
    }
  end

  def wait_distribution
    while cloud_front.get_distribution(id: config.distribution_id).distribution.status == 'InProgress'
      print '.'
      sleep 60
    end

    puts
  end

  def s3_url
    "http://#{config.bucket_name}.s3-website-#{config.region}.amazonaws.com/"
  end

  def console
    binding.pry
  end

  def create_certificate
    create_certificate_response = acm.request_certificate(
      {
        domain_name: config.target_domain,
        validation_method: "DNS"
      }
    )

    certificate_arn = create_certificate_response.certificate_arn

    domain_validation =
      acm.
        describe_certificate(certificate_arn: certificate_arn).
        certificate.
        domain_validation_options.
        first.
        resource_record

    {
      certificate_arn: certificate_arn,
      domain_validation_name: domain_validation.name,
      domain_validation_value: domain_validation.value
    }
  end

  def wait_certificate
    acm.wait_until(:certificate_validated, {certificate_arn: config.certificate_arn})
  end

  def acm
    @_acm ||= Aws::ACM::Client.new(
      access_key_id: config.access_key_id,
      secret_access_key: config.secret_access_key,
      region: 'us-east-1', # CloudFront only supports certificates in us-east-1
    )
  end

  def cloud_front
    @_cloud_front ||= Aws::CloudFront::Client.new(
      access_key_id: config.access_key_id,
      secret_access_key: config.secret_access_key,
      region: config.region
    )
  end
end
