class Config
  [
    :access_key_id,
    :secret_access_key,
    :region,
    :bucket_name,
    :target_domain,
    :site_directory,
    :distribution_id,
    :cdn_domain,
    :certificate_arn
  ].each do |field|
    define_method field do
      @store[field.to_s]
    end

    define_method "#{field}=" do |value|
      @store[field.to_s] = value
      @ini_file.write
    end
  end

  def initialize(section)
    FileUtils.mkdir_p(File.join(Dir.home, '.static-site-uploader-aws'))
    @ini_file = IniFile.new(filename: File.join(Dir.home, '.static-site-uploader-aws', 'config.ini'))
    @store = @ini_file[section]
  end

  def interactive_init
    prompt(:access_key_id)
    prompt(:secret_access_key)
    prompt(:region)
    prompt(:bucket_name, default: SecureRandom.hex(15))
    prompt(:target_domain)
    prompt(:site_directory, default: "_site")
  end

  def prompt(key, default: nil)
    key = key.to_s

    default = @store[key] || default

    if default
      print "Enter value for #{key} (default #{default}): "
    else
      print "Enter value for #{key}: "
    end

    value = STDIN.gets.strip

    if value != ''
      @store[key] = value
    else
      @store[key] = default
    end

    @ini_file.write

    value
  end

  def as_table
    Terminal::Table.new(rows: @store)
  end
end
