class StaticSiteUploaderAws
  class Commands
    attr_reader :config, :uploader

    def initialize
      @config = Config.new(Dir.pwd)
      @uploader = StaticSiteUploaderAws.new(config)
    end

    def init
      config.interactive_init
      puts "Init done:\n#{config.as_table}"
    end

    def create_bucket
      uploader.create_bucket
      puts "Created bucket #{config.bucket_name}"
    end

    def upload_site
      uploader.upload_site
      puts "Uploaded site"
    end

    def s3_url
      puts uploader.s3_url(config)
    end

    def create_distribution
      result = uploader.create_distribution
      config.distribution_id = result[:distribution_id]
      config.cdn_domain = result[:cdn_domain]

      puts "Waiting for CloudFront distribution to come alive (takes about 15 minutes)"
      uploader.wait_distribution
      puts "Created distribution. Site available at #{config.cdn_domain}. You can setup a CNAME to it now"
    end

    def invalidate_distribution
      uploader.invalidate_distribution
      puts "Distribution #{config.distribution_id} has been invalidated"
    end

    def push_site
      upload_site
      invalidate_distribution

      puts "Latest site live on #{config.cdn_domain} or #{config.target_domain}"
    end

    def create_certificate
      puts "Creating certificate. Check https://console.aws.amazon.com/acm/home?region=us-east-1#/ for details on how to validate it"
      result = uploader.create_certificate
      config.certificate_arn = result[:certificate_arn]
      puts "Created certificate for #{config.target_domain} (#{result[:certificate_arn]})"
      puts "You must add a CNAME record #{result[:domain_validation_name]} / #{result[:domain_validation_value]}"
      puts "After the CNAME is added correctly the certificate will be issued"
      uploader.wait_certificate
      puts "Certificate issued"
    end

    def final_notes
      puts "Final notes go here"
    end

    def full_setup
      init
      create_bucket
      upload_site
      create_certificate
      create_distribution
      final_notes
    end
  end
end
